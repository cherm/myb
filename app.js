var express = require('express')
var cors = require('cors')
var path = require('path')
var cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
var logger = require('morgan')

var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var loginRouter = require('./routes/login')

var app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/login', loginRouter)

const mongoose = require('mongoose')
mongoose.set('useCreateIndex', true)
mongoose.connect('mongodb+srv://admin:admin123456@cluster0-0pp5a.gcp.mongodb.net/mydb', {
  useUnifiedTopology: true,
  useNewUrlParser: true
})
const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect mongodb')
})

module.exports = app
