const User = require('../models/User')
const usersController = {
  getUsers (req, res, next) {
    User.find({}).then(function (users) {
      console.log(users)
      res.json(users)
    }).catch(function (err) {
      console.log(err)
      res.status(500).send(err)
    })
  },
  getUser (req, res, next) {
    const { id } = req.params
    User.findById(id).then(function (user) {
      res.json(user)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
}

module.exports = usersController

