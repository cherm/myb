const dbHandler = require('./db-handler')
const User = require('../models/User')

beforeAll(async () => {
  await dbHandler.connect()
})

afterEach(async () => {
  await dbHandler.clearDatabase()
})

afterAll(async () => {
  await dbHandler.closeDatabase()
})

const userComplete = {
  username: '60160121',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: 'cherm123456'
}

const usernameEmty = {
  username: '',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: 'cherm123456'
}

const passwordEmty = {
  username: '60160121',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: ''
}

const usernameandpasswordEmty = {
  username: '',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: ''
}

const username3 = {
  username: '601',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: 'cherm123456'
}

const username11 = {
  username: '12345678910',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: 'cherm123456'
}

const password3 = {
  username: '60160121',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: 'che'
}

const password21 = {
  username: '60160121',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: '123456789012345678901'
}

const username3andpassword3 = {
  username: '601',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: '123'
}

const username11andpassword21 = {
  username: '12345678910',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: '123456789012345678901'
}

const username3andpassword21 = {
  username: '123',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: '123456789012345678901'
}

const username11andpassword3 = {
  username: '12345678910',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: '123'
}

describe('User', () => {
  it('เข้าสู่ระบบได้', async () => {
    let error = null
    try {
      const user = new User(userComplete)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ username ว่าง', async () => {
    let error = null
    try {
      const user = new User(usernameEmty)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ password ว่าง', async () => {
    let error = null
    try {
      const user = new User(passwordEmty)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ username และ passwordEmty ว่าง', async () => {
    let error = null
    try {
      const user = new User(usernameandpasswordEmty)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ username น้อยกว่า 4 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(username3)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ username มากกว่ากว่า 10 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(username11)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ password มากกว่ากว่า 4 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(password3)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ password มากกว่ากว่า 20 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(password21)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ username น้อยกว่า 4 ตัวอักษร และ password น้อยกว่า 4 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(username3andpassword3)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ username มากกว่า 10 ตัวอักษร และ password มากกว่า 20 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(username11andpassword21)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ username น้อยกว่า 4 ตัวอักษร และ password มากกว่า 20 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(username3andpassword21)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ username มากกว่า 10 ตัวอักษร และ password น้อยกว่า 4 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(username11andpassword3)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
